from fabric.api import run, hosts, cd, put, local
import requests
import json
import utilstime
import datetime


def upload():
    run('supervisorctl stop tobya tobya-celeryd')
    run('pkill -9 -f tobya.py', warn_only=True)
    with cd('/home/luogni/tobya'):
        put('*.py', '.')
        put('tobya.ini', '.')
        put('requirements.txt', '.')
        put('tobya-supervisor.conf', '/etc/supervisor/conf.d/tobya.conf')
    run('chown luogni:luogni -R /home/luogni/tobya')
    run('supervisorctl reread')
    run('supervisorctl update')
    run('supervisorctl start tobya tobya-celeryd')


def dev():
    local('./tobya.py --config dev -Q tobya')
    print "celery -A tasks worker --loglevel=info"


def testevent(token=None, ts=None):
    # tsm1 = "2016-04-17 20:11:26"
    if not ts:
        ts = datetime.datetime.now() - datetime.timedelta(minutes=5 * 60)
        ts = utilstime.ts_from_dt_to_db(ts)
    host = "127.0.0.1:9123"
    # host = "188.166.64.35:9123"
    url = "http://%s/ay/%s/" % (host, token)
    ev = json.dumps({"videoendts": "20160417201156000000", "evsite": "", "text": "Movimento su Loggia su Aylook", "videocameraid": "camera:22989:sgbbJn", "eventts_unix": "1460916686", "id": "event:8503:sgbbJn", "msg_type": "alarm", "ddesc_en": "Alarm from Aylook: Movimento su Loggia su Aylook.", "action_entrypoint": "rules_beforescripts", "msg_class": "alarm", "videostartts": "20160417201121000000", "ddesc_sv": "Larm from Aylook: Movimento su Loggia su Aylook.", "camera": "22989", "aylook_id": "1", "orig.id": "", "type": "OBJECT-CHANGED", "aylook.name": "Aylook", "eventts": ts, "aylook_source_id": "1", "aylook_name": "Aylook", "aylook_obj_id": "aylook:1:sgbbJn", "objversion": "8471146", "producers": "[{\"videoendts\": \"\", \"evsite\": \"\", \"videocameraid\": \"\", \"eventts_unix\": \"1460916684\", \"id\": \"event:8502:sgbbJn\", \"ddesc_en\": \"Motion on Loggia on Aylook.\", \"permission_follow\": \"camera:22989:sgbbJn\", \"action_entrypoint\": \"\", \"videostartts\": \"\", \"ddesc_sv\": \"Motion on Loggia on Aylook.\", \"camera\": \"22989\", \"aylook_id\": \"1\", \"type\": \"OBJECT-CHANGED\", \"aylook.name\": \"Aylook\", \"eventts\": \"%s\", \"aylook_source_id\": \"1\", \"aylook_name\": \"Aylook\", \"aylook_obj_id\": \"aylook:1:sgbbJn\", \"objversion\": \"8471145\", \"evtype\": \"MOTION\", \"orig.id\": \"\", \"ddesc_it\": \"Movimento su Loggia su Aylook.\", \"name\": \"Event event:8502:sgbbJn\", \"camera_name\": \"Loggia\", \"aylook.id\": \"1\", \"objtype\": \"event\"}]" % (ts), "evtype": "MSG", "icon": "", "sound": "-1", "ddesc_it": "Allarme da Aylook: Movimento su Loggia su Aylook.", "name": "Event event:8503:sgbbJn", "ddesc": "Alarm from Aylook: Movimento su Loggia su Aylook.", "aylook.id": "1", "objtype": "event"})  # noqa
    requests.post(url, data={'event': ev})
