from time import strptime, strftime
from datetime import datetime, timedelta


gstSECOND = 1000000000
gstUSECOND = gstSECOND / 1000000


def ts_db_now():
    now = ts_ay_now()
    return ts_from_ay_to_db(now)

# checked in test56


def ts_ay_now(short=False):
    d = datetime.now()
    if short is False:
        s = ts_dt_to_ay(d)
    else:
        s = d.strftime("%Y%m%d%H%M%S")
    return s


def ts_ay_now_gst():
    dt = datetime.now()
    new = int(dt.strftime("%s")) * gstSECOND + dt.microsecond * gstUSECOND
    return new

# checked in test56


def ts_dt_to_ay(d):
    s = d.strftime("%Y%m%d%H%M%S") + "%06d" % (d.microsecond)
    return s

# checked in test56


def ts_ay_to_dt(timestamp):
    dt = datetime(*strptime(timestamp[:14], "%Y%m%d%H%M%S")[0:6])
    try:
        t = timestamp[14:]
        # check microsecond format
        if len(t) == 6:
            t = timedelta(0, 0, int(t))
            dt = dt + t
    except:
        pass

    return dt

# checked in test56


def ts_ay_diff_sec(t1, t2):
    d1 = ts_ay_to_dt(t1)
    d2 = ts_ay_to_dt(t2)

    d = (d1 - d2)

    ret = d.seconds + (d.microseconds / float(1000000))

    return ret

# checked in test56


def ts_ay_to_gst(timestamp):
    if timestamp.startswith('9999'):
        timestamp = '20300519090000'  # fix the unix time bug
    dt = ts_ay_to_dt(timestamp)
    new = int(dt.strftime("%s")) * gstSECOND + dt.microsecond * gstUSECOND
    return new


def ts_ay_to_unix(timestamp):
    return int(ts_ay_to_gst(timestamp) / gstSECOND)


def ts_dt_to_gst(dt):
    new = int(dt.strftime("%s")) * gstSECOND + dt.microsecond * gstUSECOND
    return new


def ts_gst_to_dt(timestamp):
    dt = datetime.fromtimestamp(timestamp / float(gstSECOND))
    return dt

# checked in test56


def ts_gst_to_ay(timestamp):
    try:
        dt = datetime.fromtimestamp(timestamp / float(gstSECOND))
        new = ts_dt_to_ay(dt)
    except Exception:
        new = "00000000000000000000"
    return new


def ts_gst_to_db(timestamp):
    try:
        dt = datetime.fromtimestamp(timestamp / float(gstSECOND))
        new = dt.strftime("%Y-%m-%d %H:%M:%S")
    except Exception:
        new = ""
    return new


def ts_from_ay_to_db(t):
    """Trasform a timestamp from the aylook format to the db one"""
    return "%s-%s-%s %s:%s:%s" % (t[0:4], t[4:6], t[6:8], t[8:10], t[10:12], t[12:14])


def ts_from_ay_to_db2(timestamp):
    """Trasform a timestamp from the aylook format to the db one with / like aysee uses"""
    t = strptime(timestamp[:14], "%Y%m%d%H%M%S")
    return strftime("%Y/%m/%d %H:%M:%S", t)

# checked in test56


def ts_from_db_to_ay(timestamp):
    """Trasform a timestamp from the db format to the aylook one"""
    spl = timestamp.split('.')
    t = strptime(spl[0], "%Y-%m-%d %H:%M:%S")
    try:
        ms = "%06d" % (int(spl[1]))
    except:
        ms = "000000"
    return strftime("%Y%m%d%H%M%S", t) + ms

# checked in test56


def ts_from_db_to_dt(timestamp):
    ay = ts_from_db_to_ay(timestamp)
    dt = ts_ay_to_dt(ay)
    return dt


def ts_from_dt_to_db(timestamp):
    s = timestamp.strftime("%Y-%m-%d %H:%M:%S")
    return s

# checked in test56


def ts_ay_add_seconds(timestamp, n):
    """Add n seconds to the timestamp in AyAAA format"""

    clen = 4 + 2 + 2 + 2 + 2 + 2 + 6
    if len(timestamp) < clen:
        timestamp += "0" * (clen - len(timestamp))

    dt = ts_ay_to_dt(timestamp)

    td = timedelta(seconds=n)
    td2 = dt + td

    return ts_dt_to_ay(td2)
