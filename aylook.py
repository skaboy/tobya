import conf
import ayapi
import requests
import tempfile
import os
import uuid
import json


class User:
    def __init__(self, host, username, password, chat):
        if host.find('.') < 0:
            host = "%s.aycloud.it" % host
        self.host = host
        self.chat = chat
        self.reset(host, username, password)
        self.check()

    def reset(self, host, username, password):
        self.host = host
        self.username = username
        self.password = password
        self.api = ayapi.AyAPI(host=host, user=username, password=password)

        # don't add variables here but check them in check because if i load an
        # old instance with pickle i want to add the attribute

    def check(self):
        if hasattr(self, 'token') is False:
            self.token = uuid.uuid4()
        if hasattr(self, 'dirty') is False:
            self.dirty = False
        if hasattr(self, 'chat') is False:
            self.chat = None
        if hasattr(self, 'keyboard_status') is False:
            self.keyboard_status = None
        if hasattr(self, 'profile') is False:
            self.profile = 'medium'
        if hasattr(self, 'itsmotion') is False:
            self.itsmotion = 'no'
        if hasattr(self, 'notifications') is False:
            self.notifications = 'no'
        self.api = ayapi.AyAPI(host=self.host,
                               user=self.username,
                               password=self.password,
                               profile=self.profile)

    def set_notifications(self, eventtags):
        # eventtags is list of id from ay_get_event_tags
        eventtags = ",".join(eventtags)
        t = "%s/ay/%s/" % (conf.config['postserver'], self.token)
        self.api.call('ay_object_action', 'master',
                      'notificationset', {'format': 'post',
                                          'token': t,
                                          'eventtags': eventtags,
                                          'lang': 'en',
                                          'sound': 'false'})

    def stop(self):
        self.set_notifications([])

    def get_w(self):
        if self.profile == 'max':
            return 1024
        elif self.profile == 'high':
            return 640
        elif self.profile == 'medium':
            return 320
        elif self.profile == 'low':
            return 160

    def do_setting(self, setting, v):
        d = {'no': [],
             'event': ['event'],
             'main': ['event', 'system', 'message'],
             'all': ['event', 'system', 'access_control',
                     'central_alarm', 'message', 'motion']}
        if setting == 'notifications':
            self.notifications = v
            self.set_notifications(d[self.notifications])
        elif setting == 'profile':
            self.profile = v
        return "Changed %s to %s" % (setting, v)

    def get_settings(self):
        return [[None, {'name': 'notifications',
                        'objtype': 'setting',
                        'v': self.notifications,
                        'pv': ['no', 'event', 'main', 'all']}],
                [None, {'name': 'profile',
                        'objtype': 'setting',
                        'v': self.profile,
                        'pv': ['low', 'medium', 'high', 'max']}]]

    def get_image_url(self, camera):
        u = "http://%s/perl/nph-muu-sf.php?camera=%d&username=%s&hash=%s&width=%d" % (self.host, camera, self.username, self.api.hash, self.get_w())  # noqa
        return u

    def get_camera_list(self):
        return self.get_list('camera')

    def got_event(self, ev):
        self.last_event = json.loads(ev)

    def get_last_event(self):
        fname = self.get_event_image(self.last_event)
        yield (fname, self.last_event)

    def _return_camera_live(self, oid):
        c = self.api.ol(['camera'], fields=['id', 'db.id', 'objtype',
                                            'name', 'camera_status'],
                        ids=[oid])[0]
        return [(self.get_image_for_camera(c), c)]
        
    def get_filter_event(self, filtermode, arg, offset):
        offset = int(offset)
        if offset < 0:
            if filtermode == 'camera':
                return self._return_camera_live(arg)
            else:
                return []

        filters = ""
        if filtermode == 'camera':
            filters = '($videocameraid like "%s")' % (arg)
            
        ret = self.api.ol(['event'],
                          sort_field='eventts', sort_how='desc',
                          offset=offset, limit=1,
                          fields=['eventts', 'ddesc', 'objtype', 'eventts_unix',
                                  'videocameraid', 'videostartts',
                                  'camera', 'videoendts'],
                          filters=filters)
        print (ret)
        if ret:
            ret[0]['__cmdtype'] = 'filterevent'
            ret[0]['__filtermode'] = filtermode
            ret[0]['__arg'] = arg
            ret[0]['__offset'] = offset
            return [(self.get_event_image(ret[0]), ret[0])]
        if filtermode == 'camera':
            return self._return_camera_live(arg)
        else:
            return []

    def get_list(self, objtype):
        if objtype == 'camera':
            return self.api.ol(['camera'], fields=['id', 'db.id', 'objtype',
                                                   'name', 'camera_status'],
                               sort_field=['name'], sort_how='asc')
        elif objtype == 'event':
            return self.api.ol(['event'],
                               sort_field='eventts', sort_how='desc',
                               offset=self._asked_offset,
                               limit=5,
                               fields=['eventts', 'ddesc', 'objtype',
                                       'videocameraid', 'videostartts',
                                       'camera',
                                       'videoendts'])
        else:
            return self.api.ol(['camera', 'partition', 'panel', 'zone',
                                'io'],
                               filters='($name ilike "%s")' % objtype,
                               sort_field=['name'], sort_how='asc',
                               offset=self._asked_offset,
                               limit=20,
                               fields=['objtype', 'name',
                                       'camera_status',
                                       'iotype', 'value'])

    def get_obj_list(self, objtype):
        ret = self.get_list(objtype)
        for o in ret:
            yield (None, o)

    def get_events(self):
        evs = self.get_list('event')
        for e in evs:
            fname = self.get_event_image(e)
            yield (fname, e)
            if fname:
                os.unlink(fname)

    def get_image_for_camera(self, c):
        if c['camera_status'] in ['DISCONNECTED', 'FAIL']:
            return None
        u = self.get_image_url(int(c['db.id']))
        ret = requests.get(u)
        fname = None
        if ret.status_code == 200:
            with tempfile.NamedTemporaryFile('wb',
                                             suffix=".jpg",
                                             delete=False) as f:
                f.write(ret.content)
                fname = f.name
        return fname
        
    def get_image_for_cameras(self, cameras=None):
        if cameras is None:
            cameras = self.get_camera_list()
        for c in cameras:
            img = self.get_image_for_camera(c)
            yield (img, c)
            if img:
                os.unlink(img)

    def get_event_image(self, ev):
        if '__aybot_img' in ev:
            return ev['__aybot_img']
        return self.api.get_event_image(ev, self.get_w())

    def get_console(self):
        btns = sorted(self.api.call('ay_get_button_console'),
                      key=lambda k: int(k['layout_seq']))
        for b in btns:
            yield (None, b)

    def console_click(self, btn):
        self.api.call('ay_button_click', btn)

    def xay_install(self, url, conf):
        self.api.call('ay_object_action', 'master', 'task',
                      {'task': 'aypy.xay.extensions.install',
                       'params': "|".join([url, conf])})

    def xay_list(self):
        try:
            ret = self.api.call('ay_object_action', 'master', 'task',
                                {'task': 'aypy.xay.extensions.list',
                                 'params': ''})
            return ret[0]['xaylist'].split(',')
        except:
            return []

    def xay_delete(self, name):
        self.api.call('ay_object_action', 'master', 'task',
                      {'task': 'aypy.xay.extensions.delete',
                       'params': name})

    def disable_scenarios(self, name=None):
        for s in self.api.ol(['rule'], filters='($name like "scenario.")',
                             fields=['id', 'name']):
            if (name is None)or("scenario.%s" % name == s['name']):
                self.api.call('ay_object_remove', s['id'])

    def list_scenarios(self):
        a = self.api.ol(['aylook'],
                        filters=('($aylook.master == "master")\
                        and($master == "master")'),
                        fields=['id', 'x-scenarios'])
        if a[0].get('x-scenarios'):
            a = json.loads(a[0]['x-scenarios'])
            for s in a:
                s['active'] = self.is_scenario_active(s['rulename'])
                yield (None, s)

    def is_scenario_active(self, rname):
        for s in self.api.ol(['rule'], filters='($name like "scenario.")',
                             fields=['id', 'name']):
            if s['name'] == rname:
                return True
        return False

    def activate_scenario(self, sname):
        self.disable_scenarios(sname)
        for (img, s) in self.list_scenarios():
            if s['name'] == sname:
                self.api.call("ay_object_add", "RULE",
                              {'name': s['rulename'],
                               'script': s['script']})

    def deactivate_scenario(self, sname):
        self.disable_scenarios(sname)

    def save_scenarios(self, ss):
        ss = json.dumps(ss)
        self.api.call('ay_object_action', 'master', 'set.conf',
                      {'x-scenarios': ss})

    def process_document(self, doc):
        data = None
        with tempfile.NamedTemporaryFile('wb',
                                         suffix=".txt") as f:
            doc.save(f.name)
            with open(f.name, "r") as ff:
                data = ff.read()
        if not data:
            return
        modes = []
        mode = {}
        self.disable_scenarios()
        for line in data.split('\n'):
            if line.startswith('#'):
                if mode:
                    modes.append(mode.copy())
                mode['name'] = line[1:].strip()
                mode['rulename'] = 'scenario.%s' % (mode['name'])
                mode['script'] = ""
            else:
                mode['script'] += line + "\n"
        if mode:
            modes.append(mode)
        self.save_scenarios(modes)
