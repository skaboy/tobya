try:
    import ujson
except:
    import json as ujson
import requests
import urllib.request
import urllib.parse
import urllib.error
import hashlib
import random
from time import strptime, strftime
import tempfile
import base64
import os


def ts_from_db_to_ay(timestamp):
    """Trasform a timestamp from the db format to the aylook one"""
    spl = timestamp.split('.')
    t = strptime(spl[0], "%Y-%m-%d %H:%M:%S")
    try:
        ms = "%06d" % (int(spl[1]))
    except:
        ms = "000000"
    return strftime("%Y%m%d%H%M%S", t) + ms


def call(*args, **kwargs):
    a = AyAPI(user='')
    return a.call(*args, **kwargs)


def from_srcdata(srcdata):
    return AyAPI(srcdata['host'], srcdata['port'],
                 srcdata['user'], srcdata['password'])


class AyAPI:
    def __init__(self, host="127.0.0.1", port=80,
                 user='admin', password='admin', token='',
                 profile='medium'):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        if not token:
            m = hashlib.md5()
            k = user + ":" + password
            m.update(k.encode('utf-8'))
            self.hash = m.hexdigest()
        else:
            self.user = token
            self.hash = ''
        self.profile = profile

    def to_srcdata(self):
        return {'host': self.host, 'port': self.port,
                'user': self.user, 'password': self.password}

    def cid_to_dbid(self, cid):
        c = self.ol(['camera'], ids=[cid], fields=['db.id'])
        if c:
            return int(c[0]['db.id'])

    def get_mp4_from_frames(self, cid, ev, w):
        ts = ts_from_db_to_ay(ev['eventts'])
        frames = self.call('ay_archive_get_frames_step',
                           cid, ts, 2, 4, 1.0, 1.0,
                           'video/jpeg,width=%d' % w)
        flists = []
        for frame in frames:
            with tempfile.NamedTemporaryFile('wb',
                                             suffix=".jpg",
                                             delete=False) as f:
                f.write(base64.b64decode(frame['data']))
                flists.append(f.name)
        if not flists:
            return None
        with tempfile.NamedTemporaryFile('wb',
                                         suffix=".mp4",
                                         delete=False) as f:
            os.system('cat %s | ffmpeg -y -f image2pipe -i - -framerate 5 %s' %
                      (" ".join(flists), f.name))
            fname = f.name
        for f in flists:
            os.unlink(f)
        return fname

    def get_mp4_from_export(self, cid, ev, w):
        vts = ev['videostartts']
        vte = ev['videoendts']
        u = self.get_url("exportstd", cid, "video/aylook,profile=%s" % self.profile,
                         tstart=vts, tsend=vte)
        with tempfile.NamedTemporaryFile('wb',
                                         suffix=".mp4",
                                         delete=False) as f:
            cmd = "gst-launch-1.0 souphttpsrc location=\"%s\" ! gdpdepay ! avdec_h264 ! x264enc ! mp4mux ! filesink location=%s" % (u, f.name)
            print ("Running", cmd)
            os.system(cmd)
            fname = f.name
        return fname

    def get_event_image(self, ev, w=640):
        cid = None

        for k in ['eventts', 'videocameraid', 'videostartts', 'videoendts']:
            if (k not in ev)or(not(ev[k])):
                return
        
        for c in ['videocameraid']:
            if (c in ev)and(ev[c]):
                cid = ev[c]
                break
        
        if cid is None:
            return
        cid = self.cid_to_dbid(cid)

        return self.get_mp4_from_export(cid, ev, w)
        
    def get_url(self, mode, cid, caps, tstart=None, tsend=None):
        uuid = "AYG" + str(random.randint(0, 999))
        url = "http://%s:%d/%s?camera_id=%d&user=%s&hash=%s&caps=%s&uuid=%s" % (self.host, int(self.port) + 1, mode, int(cid), self.user, self.hash, caps, uuid)  # noqa
        if tstart:
            url += "&time_start=%s&time_end=%s" % (tstart, tsend)
        return url

    def ol(self, types='', ids='', ays='',
           sort_field='', sort_how='asc', offset="", limit="",
           filters="", filter_ay_status="",
           how="hash", fields="", follow=""):
        return self.call("ay_object_list",
                         types, ids, ays,
                         sort_field, sort_how, offset, limit,
                         filters,
                         filter_ay_status,
                         how, fields,
                         follow)

    def call(self, *args, **kwargs):
        decode = kwargs.get('decode', True)
        dump = kwargs.get('dump', False)
        onlydata = kwargs.get('onlydata', True)
        user = kwargs.get('username', self.user)
        hash_ = kwargs.get('hash', self.hash)
        stream = kwargs.get('stream', False)
        batch_add_user = kwargs.get('batch_add_user', False)
        if (user == ""):
            hash_ = ""
        if isinstance(args[0], str):
            m = args[0]
            args = (user, hash_) + args[1:]
            aa = ujson.dumps(args)
            params = {'method': m}
            for k, v in list(kwargs.items()):
                if k not in ['decode', 'dump', 'onlydata']:
                    if isinstance(v, str):
                        params[k] = v
                    else:
                        params[k] = ujson.dumps(v)
        else:
            bau = []
            if batch_add_user is True:
                bau = [user, hash_]
            pp = [(m[0], bau + m[1:]) for m in args]
            params = {'batch': ujson.dumps(pp)}
        url = "http://%s:%d/ayapi?%s" % (self.host, self.port,
                                         urllib.parse.urlencode(params))
        if dump is True:
            return url
        if ('batch' in params):
            ret = requests.get(url, stream=stream, timeout=30)
        else:
            ret = requests.post(url, data={'args': aa},
                                params={"username": user, "hash": hash_},
                                stream=stream, timeout=30)
        if decode:
            ret = ujson.loads(ret.text)
            if ret['success'] is False:
                print (ret)
            if onlydata:
                return ret["data"]
        return ret
