from bottle import post, run, request, default_app


@post('/ay/<token>/')
def aypost(token=None):
    if not token:
        return
    q = request.app._web_queue
    ev = request.forms.get('event')
    q.put((token, ev))
    return ''


def start(q):
    default_app()._web_queue = q
    run(host='0.0.0.0', port=9120, debug=True)
