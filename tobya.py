import multiprocessing
import web
import aylook
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          CallbackQueryHandler,
                          ConversationHandler, PicklePersistence)
from telegram.ext.dispatcher import run_async
import logging
import conf
import queue
import tasks
import talk
import argparse
import utilstime
import json

# TODO
#  - import scenarios
#  - emoji shortcuts (do action, send emoji, save as shortcut in main keyboard, remove by sending another command after pressing shortcut)
#  - panel (pin on setting page)
#  - more settings (event types for notifications?)
#  - shorter video files? maybe as option (or image)
#  - aylook to digital ocean custom image and raspberry tunnels
#  - report diagnostic as event
#  - rules / scenari: better doc, file and so on
#  - add merano home support
#     - see Temp Hum Mode (Heater?)
#     - set Target (on/off -> 21/10 ok)
#     - use diagnostics? extensions? only rules as now?
#     - idea now is to use rule to create a py file and use it with task api
#     - or make a rule file to create a task to install extensions and use it
#       to download
#       extensions and install them from them. Use extension to create std gui.
#     - make a button that if clicked will report current merano-home status
#     - make another button to change current status (cycle?)
#  - github / gitlab
#  - hide token/passwords from git
#  - better doc
#  - KISS
#  - fab -> ansible


# TODO AYLOOK
#  - all camera on luogni2
#  - luogni2 on cloud
#  - raspberry tunnel/temp/hum/relay/modbus/watchdog
#  - amazon rekognition api
#  - imavis lics
#  - use imavis rect to select frames/get motion thumbnail/video synopsis/daily summary

# SCENARIOS
#  - you can send a .rules file to bot and it'll be a list of scenarios
#    to enable/disable
#  - each scenario is a single rule
#  - scenarios are saved in master.x-scenarios as json
#  - active scenario are saved as RULE object with name = scenario.name

# EXTENSIONS
#  - you can install the extension scenario and activate it
#  - this will provide some commands (/xayinstall /xaydelete /xaylist)
#  - "/xaylist" will list installed extensions and their conf
#  - "/xayinstall url conf" will install xay from url and use conf for it
#  - "/xaydelete name" will remove "name" xay
#  - each extension is made using xay framework (fab deb.extension)
#  - each extension is also installed in /opt/aylook/xay/
#  - save conf there as json
#  - provide maybe an url on aylook (webpages) with conf/setup/output?

# TOMO
#  - https://raymii.org/s/tutorials/Autossh_persistent_tunnels.html
#  - create account
#  - create server
#  - network watchdog
#  - temp / hum sensor
#  - relay
#  - tcp tunnel ()
#    - systemd / enable at boot
 
# FEATURES
#  done
#  - easy login and support multiple logins
#  - notifications with video in chat and android notifications (telegram X)
#  - backup of events (video)
#  - console (status / click)
#  - camera images
#  - image notification in all kind of events
#  todo
#  - virtual patrol? (like every days send X seconds from each camera/group)
#  - payments?
#  - advanced analysis for motion?
#  - periodic reports (stats, ..)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

BOTQUEUE = multiprocessing.Queue()
TASKS = []


# possible states
DOINGLOGIN, MAINMENU = range(2)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def done(bot, update, user_data):
    update.message.reply_text('Done!', reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def start(bot, update):
    return do_login(bot, update, {})


def get_ay(user_data):
    try:
        return user_data['aylist'][user_data['aycounter']]
    except:
        return None


def go_to_main_menu(bot, update, user_data, msg='Welcome!'):
    user = get_ay(user_data)
    if not user:
        return do_login(bot, update, user_data)
    print (user, user.host, user.token)
    lname = 'login(%s)' % user.host
    k = [[lname],
         ['console', 'scenarios'],
         ['camera', 'events'],
         [],
         ['settings'],
         ['logout', 'newlogin']]
    markup = ReplyKeyboardMarkup(k)
    update.message.reply_text(msg, reply_markup=markup)
    return MAINMENU


def do_reload(bot, update, user_data):
    return go_to_main_menu(bot, update, user_data, msg='reload')


def do_logout(bot, update, user_data):
    u = get_ay(user_data)
    if not u:
        return do_login(bot, update, user_data)
    del user_data['aylist'][user_data['aycounter']]
    u.stop()
    if not user_data['aylist']:
        return start(bot, update)
    user_data['aycounter'] = 0
    return go_to_main_menu(bot, update, user_data)


def do_login(bot, update, user_data):
    update.message.reply_text('Hi! Please write your aylook login information separated by space: cloudname username password.', reply_markup=ReplyKeyboardRemove())
    return DOINGLOGIN


def dologin(bot, update, user_data):
    try:
        (cloudname, user, password) = update.message.text.split(' ')
    except:
        return start(bot, update)
    if 'aylist' not in user_data:
        user_data['aylist'] = []
    u = aylook.User(cloudname, user, password, update.message.chat_id)
    user_data['aylist'].append(u)
    user_data['aycounter'] = len(user_data['aylist']) - 1
    return go_to_main_menu(bot, update, user_data)


def next_login(bot, update, user_data):
    user_data['aycounter'] = user_data['aycounter'] + 1
    if user_data['aycounter'] >= len(user_data['aylist']):
        user_data['aycounter'] = 0
    return go_to_main_menu(bot, update, user_data)


# remember that the function returning new conversation status
# should not be @run_async-ed
@run_async
def _real_show_objects(bot, update, u):
    k = update.message.text
    (fnlist, fncaption) = (None, None)
    args = {}
    if k == 'camera':
        (fnlist, fncaption) = (u.get_image_for_cameras, talk.get_obj_caption)
    elif k == 'console':
        (fnlist, fncaption) = (u.get_console, talk.get_console_caption)
        args = {'merge_cmds': True, 'num_cols': 3}
    elif k == 'scenarios':
        (fnlist, fncaption) = (u.list_scenarios, talk.get_scenario_caption)
        args = {'merge_cmds': True, 'num_cols': 2}
    elif k == 'events':
        (fnlist, fncaption) = (u.get_filter_event, talk.get_obj_caption)
        args = {'fnlistargs': ['events', '', 0]}
    if fnlist:
        talk.send(bot, u, fnlist, fncaption, **args)
    

def show_objects(bot, update, user_data):
    u = get_ay(user_data)
    if not u:
        return do_login(bot, update, user_data)
    _real_show_objects(bot, update, u)
    return go_to_main_menu(bot, update, user_data, msg='done!')


@run_async
def _real_list_settings(bot, update, u):
    k = update.message.text.split('(')[0]
    if k == 'settings':
        talk.send(bot, u, u.get_settings, talk.get_obj_caption)
    

def list_settings(bot, update, user_data):
    u = get_ay(user_data)
    if not u:
        return do_login(bot, update, user_data)
    _real_list_settings(bot, update, u)
    return go_to_main_menu(bot, update, user_data, msg='settings')


@run_async
def _real_do_callback(bot, update, user_data):
    u = get_ay(user_data)
    if not u:
        return
    query = update.callback_query
    print ("CB", query.data, query.message.text)
    try:
        (cmd, args) = query.data.split("|")
    except:
        pass
    (fn, fnlist, fncaption) = (None, None, None)
    if cmd == 'buttonclick':
        fn = u.console_click
    elif cmd == 'activatescenario':
        fn = u.activate_scenario
    elif cmd == 'deactivatescenario':
        fn = u.deactivate_scenario
    elif cmd == 'filterevent':
        (fnlist, fncaption) = (u.get_filter_event, talk.get_obj_caption)
    elif cmd == 'setting':
        fn = u.do_setting
    if fn:
        reply = fn(*args.split(','))
    if fnlist:
        talk.send(bot, u, fnlist, fncaption, edit=query.message, fnlistargs=args.split(','))
    if reply:
        bot.send_message(u.chat, reply, timeout=10)
    query.answer()


def do_callback(bot, update, user_data):
    _real_do_callback(bot, update, user_data)
    return None


def get_user_from_token(pp, token):
    shared = pp.get_user_data()
    for k, u in shared.items():
        if 'aylist' in u:
            for ay in u['aylist']:
                if str(ay.token) == token:
                    return ay
    return None


def flush_persistance(bot, job):
    print ("flush persistance")
    job.context.flush()


def check_events(bot, job):
    global TASKS

    while True:
        try:
            (token, ev) = BOTQUEUE.get_nowait()
        except queue.Empty:
            break
        u = get_user_from_token(job.context, token)
        if u:
            ret = tasks.handle_event.delay(u.api.to_srcdata(),
                                           ev, token, u.get_w())
            TASKS.append(ret)

    new = []
    for t in TASKS:
        if t.ready():
            try:
                (token, ev) = t.result
            except:
                continue
            u = get_user_from_token(job.context, token)
            if u:
                u.got_event(ev)
                talk.send(bot, u, u.get_last_event, talk.get_obj_caption)
        else:
            new.append(t)
    TASKS = new


def main():
    # run as python3 tobya.py --config MODE (or use fab)
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='cloud')
    parser.add_argument('--test')
    parser.add_argument('--token')
    args = parser.parse_args()

    logger.info("Starting with %s config", args.config)
    conf.init(args.config)

    pp = PicklePersistence(filename='tobya', on_flush=True)
    updater = Updater(conf.config['apikey'], persistence=pp)
    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    j = updater.job_queue

    # now check all User class
    for k, u in pp.get_user_data().items():
        if 'aylist' in u:
            for ay in u['aylist']:
                ay.check()

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start),
                      RegexHandler('^login$', start)],

        states={
            DOINGLOGIN: [MessageHandler(Filters.text, dologin, pass_user_data=True)],
            
            MAINMENU: [RegexHandler('^camera$', show_objects, pass_user_data=True),
                       RegexHandler('^console$', show_objects, pass_user_data=True),
                       RegexHandler('^scenarios$', show_objects, pass_user_data=True),
                       RegexHandler('^events$', show_objects, pass_user_data=True),
                       RegexHandler('^login', next_login, pass_user_data=True),
                       RegexHandler('^logout', do_logout, pass_user_data=True),
                       RegexHandler('^newlogin', do_login, pass_user_data=True),
                       RegexHandler('^settings', list_settings, pass_user_data=True),
                       CallbackQueryHandler(do_callback, pass_user_data=True)]
        },

        fallbacks=[RegexHandler('^Done$', done, pass_user_data=True)],

        run_async_timeout=60,
        name='tobyconv',
        persistent=True
    )

    dp.add_handler(conv_handler)

    dp.add_error_handler(error)

    updater.start_polling()

    if args.test is None:
        p = multiprocessing.Process(target=web.start, args=(BOTQUEUE,))
        p.start()
        j.run_repeating(check_events, interval=1, context=pp)
        j.run_repeating(flush_persistance, interval=60, context=pp)
        updater.idle()
    else:
        u = get_user_from_token(pp, args.token)
        (fnlist, fncaption) = (None, None)
        if args.test == 'console':
            (fnlist, fncaption) = (u.get_console, talk.get_console_caption)
            args = {'merge_cmds': True, 'num_cols': 4}
        elif args.test == 'event':
            ts = utilstime.ts_ay_now()
            ts = "20181116134556"
            u.got_event(json.dumps({"videoendts": utilstime.ts_ay_add_seconds(ts, +5),
                                    "videostartts": utilstime.ts_ay_add_seconds(ts, -5),
                                    "videocameraid": "camera:22994:sgbbJn",
                                    "eventts": utilstime.ts_from_ay_to_db(ts),
                                    "ddesc": "test event"}))
            (fnlist, fncaption) = (u.get_last_event, talk.get_obj_caption)
        elif args.test == 'scenarios':
            (fnlist, fncaption) = (u.list_scenarios, talk.get_scenario_caption)
            args = {'merge_cmds': True, 'num_cols': 2}

        if fnlist:
            talk.send(updater.bot, u, fnlist, fncaption, **args)


if __name__ == '__main__':
    main()
