import configparser

config = None


def init(mode):
    global config
    CONFIG = configparser.ConfigParser()
    CONFIG.read("tobya.ini")
    config = CONFIG[mode]
