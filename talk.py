import emoji
from telegram import (InlineKeyboardButton, InlineKeyboardMarkup, ParseMode)


def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu


def send(bot, u, fnlist, fncaption, fnlistargs=[],
         merge_cmds=False, num_cols=4, edit=None):
    cmds = []
    for (img, c) in fnlist(*fnlistargs):
        try:
            (k, t) = fncaption(c)
            if t:
                t = emoji.emojize(t, use_aliases=True)
            markup = None
            keyboard = []
            skip = False
            if k:
                for (kname, kcbdata) in k:
                    if merge_cmds is False:
                        keyboard.append(InlineKeyboardButton(emoji.emojize(kname, use_aliases=True),
                                                             callback_data=kcbdata))
                    else:
                        cmds.append(InlineKeyboardButton(emoji.emojize(t, use_aliases=True),
                                                         callback_data=kcbdata))
                        skip = True
            if skip:
                continue
            if keyboard:
                markup = InlineKeyboardMarkup(build_menu(keyboard, num_cols))
            if img:
                with open(img, 'rb') as f:
                    if img.endswith('.jpg'):
                        bot.send_photo(u.chat, f, caption=t, reply_markup=markup,
                                       parse_mode=ParseMode.MARKDOWN,
                                       timeout=10)
                    elif img.endswith('.mp4'):
                        bot.send_video(u.chat, f, caption=t, reply_markup=markup,
                                       parse_mode=ParseMode.MARKDOWN,
                                       supports_streaming=True,
                                       timeout=30)
            elif (t):
                bot.send_message(u.chat, t, reply_markup=markup,
                                 parse_mode=ParseMode.MARKDOWN,
                                 timeout=10)
        except Exception as e:
            print ("error in send -", str(e))

    if cmds:
        try:
            markup = InlineKeyboardMarkup(build_menu(cmds, num_cols))
            bot.send_message(u.chat, "Commands", reply_markup=markup,
                             parse_mode=ParseMode.MARKDOWN)
        except Exception as e:
            print ("error in send cmds -", str(e))


def _cb(name, otype, args):
    return [name, "%s|%s" % (otype, ",".join([str(a) for a in args]))]


def get_obj_caption(o):
    E = {'camera_status_VISUAL': ':smile:',
         'camera_status_FAIL': ':worried:',
         'camera_status_REC': ':smile:',
         'camera_status_DISCONNECTED': ':sleeping:',
         'io_value_0': ':tomato:',
         'io_value_1': ':tennis:',
         'set0': '',
         'set1': ':punch: '}
    otype = o.get('objtype')
    cb = None
    if otype == 'camera':
        t = o['name'] + E.get('camera_status_' + o['camera_status'], "")
        cb = [_cb("lastevent", "filterevent", ['camera', o['id'], 0])]
    elif otype == 'event':
        t = o.get('eventts') + " " + o.get('ddesc')
        if o.get('__cmdtype') == 'filterevent':
            cb = [_cb("prev", "filterevent", [o['__filtermode'], o['__arg'], o['__offset'] + 1]),
                  _cb("next", "filterevent", [o['__filtermode'], o['__arg'], o['__offset'] - 1])]
            
    elif otype == 'io':
        if o['iotype'] == 'digital':
            t = o['name'] + E.get('io_value_' + o['value'], "")
        else:
            t = o['name'] + " " + o['value']
    elif otype == 'setting':
        t = "%s (%s)" % (o['name'], o['v'])
        cb = [_cb(E['set%d' % int((pv == o['v']))] + pv,
                  "setting", [o['name'], pv]) for pv in o['pv']]
    else:
        t = o.get('name', "")
    return (cb, t)


def get_console_caption(c):
    E = {'0': ':tomato:',
         '1': ':tennis:'}
    t = ""
    cb = None
    if c['button_id'] != '':
        t = ":zap: %s" % (c['button_name'])
        cb = [_cb("click", "buttonclick", [c['button_id']])]
    elif c['diagnostic_id'] != '':
        dname = c['linked_object_name']
        if not dname:
            dname = c['diagnostic_name']
        if c['diagnostic_type'] == 'analog':
            t = "*%s*: %s" % (dname, c['diagnostic_value'])
        else:
            t = "*%s*: %s" % (dname, E[c['diagnostic_value']])
    return (cb, t)
        

def get_scenario_caption(s):
    if s['active']:
        t = s['name'] + ":tennis:"
        cb = [_cb("", "deactivatescenario", [s['name']])]
    else:
        t = s['name'] + ":tomato:"
        cb = [_cb("", "activatescenario", [s['name']])]
    return (cb, t)
