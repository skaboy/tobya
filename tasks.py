from celery import Celery
import json

# celery -A tasks worker --loglevel=info

app = Celery('tasks')
app.config_from_object('celeryconfig')


@app.task(bind=True)
def handle_event(self, srcdata, evj, token, w):
    print ("handle event for", token, w)
    ev = json.loads(evj)
    # api = ayapi.from_srcdata(srcdata)
    # img = api.get_event_image(ev, w)
    # if img:
    #     ev['__aybot_img'] = img
    return (token, json.dumps(ev))
